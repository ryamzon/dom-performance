vuser_end()
{
	/* Logout */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_Logout"));

	web_url("logout", 
		"URL={pURL}/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t16.inf", 
		"Mode=HTML", 
		LAST);

	web_url("miplogout", 
		"URL={pURL_SSO}/miplogout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t17.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_Logout"),LR_AUTO); 

	return 0;
}