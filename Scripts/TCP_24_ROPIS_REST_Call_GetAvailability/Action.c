Action()
{

	web_set_user("TESTSUPERUSER2", "Place2017!","10.18.3.185:30000");
		
	web_url("soapui-updates-os.xml", 
		"URL=http://dl.eviware.com/version-update/soapui-updates-os.xml", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		LAST);

	web_add_header("Authorization","Basic VEVTVFNVUEVSVVNFUjI6UGxhY2UyMDE3IQ==");
	
	lr_think_time(1);
			
	lr_start_transaction("TCP_24_ROPIS_REST_Call_GetAvailability");
	
	//web_reg_find("Text=atcStatus");

	web_custom_request("getAvailabilityList", 
		"URL=http://10.18.3.185:30000/services/atc/availability/getAvailabilityList", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={\n   \"availabilityRequest\":{\n      \"viewName\":\"US ROPIS\",\n      \"availabilityCriteria\":{\n         \"itemNames\":{\n            \"itemName\":[\n               \"{pItemName}\"\n            ]\n         },\n         \"facilityNames\":{\n            \"facilityName\":[\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\",\n               \"{pFacilityID}\"\n            ]\n         }\n      }\n   }\n}", 
		LAST);

	lr_end_transaction("TCP_24_ROPIS_REST_Call_GetAvailability", LR_AUTO);


	return 0;
}