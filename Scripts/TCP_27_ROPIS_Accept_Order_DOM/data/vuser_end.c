vuser_end()
{

	lr_start_transaction("Logout");

	web_url("user.png", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/mps/resources/icons/64/user.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/index.html", 
		"Snapshot=t20.inf", 
		LAST);

	web_url("default-medium-arrow.png", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/images/button/default-medium-arrow.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/ext-theme-elemental-all.css", 
		"Snapshot=t21.inf", 
		LAST);

	web_url("mouse.png", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/mps/resources/icons/mouse.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/mps-elemental.css", 
		"Snapshot=t22.inf", 
		LAST);

	web_url("checkbox.png", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/images/form/checkbox.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/ext-theme-elemental-all.css", 
		"Snapshot=t23.inf", 
		LAST);

	web_url("logout", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/index.html", 
		"Snapshot=t24.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=https://njiqlropapp01.tcphq.tcpcorp.local.com:15001/images/loading.gif", "Referer=https://njiqlropapp01.tcphq.tcpcorp.local.com:15001/manh/resources/css/mip.css", ENDITEM, 
		LAST);

	web_reg_find("Text=Sign Out | Manhattan Associates Inc.", 
		LAST);

	web_url("miplogout", 
		"URL=https://njiqlropapp01.tcphq.tcpcorp.local.com:15001/miplogout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t25.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("Logout",LR_AUTO);

	lr_think_time(3);

	return 0;
}