Action()
{
	
	/* View Store Orders */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_View_Store_orders"));

/*	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1479378229410", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/resources/css/elemental/images/tools/tool-sprites.png", "Referer={pURL}/manh/resources/css/elemental/ext-theme-elemental-all.css", ENDITEM, 
		"Url=../manh/resources/css/elemental/images/tab-bar/default-plain-scroll-right.png", "Referer={pURL}/manh/resources/css/elemental/ext-theme-elemental-all.css", ENDITEM, 
		"Url=../manh/resources/css/elemental/images/tab-bar/default-plain-scroll-left.png", "Referer={pURL}/manh/resources/css/elemental/ext-theme-elemental-all.css", ENDITEM, 
		"Url=../services/rest/lps/MenuService/menu/item?_dc=1479378237646&id=1280142", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/mps/resources/icons/16/default-icon.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST); */
		
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-7981426&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);

	
	/*Extract MANH-CSRFToken value from response. Original Value: "e4jAFq3RYctlYkQmfSN5+Zwgw6yiEOLCOT6LUMa1cIk="*/
		
	web_reg_save_param("cMANH_CSRFToken",
	                   "LB=<input id=\"MANH-CSRFToken\" type=\"hidden\" name=\"MANH-CSRFToken\" value=\"",
	                   "RB=\"/><script",
	                   "ORD=1",
	                   LAST);
	
	/*Extract View State value from response. Orginial Value: 4712175963253169600:-3939968574676408262*/
	web_reg_save_param("cViewState_01",
	                   "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
	                   "RB=\" autocomplete=\"",
	                   "ORD=1",
	                   LAST);
	
	/*Extract DataTable value from response. Original Value: $1479378236740*/
	web_reg_save_param("cDataTable",
	                   "LB=dataTable$:$",
	                   "RB=\" />",
	                   "ORD=1",
	                   LAST);
	
	/*Extract Filter Name value from response. Original Value: FL_mboban*/
	web_reg_save_param("cFilterName",
	                   "LB=name=\"dataForm:fltrListFltrId:filterName\" value=\"",
	                   "RB=\" />",
	                   "ORD=1",
	                   LAST);
	
	/*Extract Filter Owner value from response. Original Value: mboban*/
	web_reg_save_param("cFilterOwner",
	                   "LB=name=\"dataForm:fltrListFltrId:owner\" value=\"",
	                   "RB=\" />",
	                   "ORD=1",
	                   LAST);
	
	/*Extract StoreAliasID value from response. Original Value: 4242*/
/*	web_reg_save_param("cStoreAliasID",
	                   "LB=document.forms[this.form.id].addParam('storeAliasIdFromBOPIS','",
	                   "RB=');",
	                   "ORD=1",
	                   LAST); */
	
	/*Extract IsCancel value from response. Original Value: true*/
	web_reg_save_param("cIsCancel",
	                   "LB=hidden\" name=\"isCancel\" value=\"",
	                   "RB=\"",
	                   "ORD=ALL",
	                   LAST);
	
		/*Extract LandingPage_DistributionID value from response*/
	web_reg_save_param("cLandingPage_DistributionID",
                   "LB=tcId8\">",
                   "RB=<",
                   "ORD=ALL",
                   LAST);
	
	
	/*Extract OrderStatusCode value from response*/
	web_reg_save_param("cOrderStatusCode",
	                   "LB=orderStatusCode\" value=\"",
	                   "RB=\"",
	                   "ORD=ALL",
	                   LAST);
	
	
	/*Extract Picklist value from response*/
	web_reg_save_param("cPK",
                   "LB=/><input type=\"hidden\" value=\"",
                   "RB=\"",
                   "ORD=ALL",
                   LAST);

	web_url("InStorePickupSolution.xhtml", 
		"URL={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=InStorePickUp.js", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/inputmask/scripts/mask.js", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/rightclickmenu/scripts/rightclickmenu.js", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/menu/images/foPrint.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/layout/images/backdnew.png", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/manh/mps/resources/icons/funnel.png", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/dialogControl/images/close.png", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/pinfavapp.png", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=images/CustomerPickup.png", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=images/ShipFromStore.png", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=images/Alerts.png", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lcom/common/image/softcheck.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=images/Notes.png", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/menu/ribbon/images/home1.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/menu/images/clear.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/customize.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/foPrint.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/menu/ribbon/images/help.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/arrow_collapse.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/arrow_first_disabled.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/arrow_left_disabled.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/arrow_right_disabled.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/arrow_last_disabled.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/refresh.gif", "Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", ENDITEM, 
		"Url=/lps/resources/common/images/arrow-black.gif", "Referer={pURL}/lps/lpsstyles", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/sortup.gif", "Referer={pURL}/lps/lpsstyles", ENDITEM, 
		"Url=/lps/resources/common/images/dropdown.png", "Referer={pURL}/lps/lpsstyles", ENDITEM, 
		"Url=/ext/resources/ext-theme-neptune/images/tools/tool-sprites.png", "Referer={pURL}/lps/lpsstyles", ENDITEM, 
		"Url=/lps/resources/panel/images/rule1.gif", "Referer={pURL}/lps/resources/layout/css/LPSLayoutStyles_firefox.css", ENDITEM, 
		LAST);

	web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1479378246404", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_View_Store_orders"),LR_AUTO);

	lr_think_time(1);

		/*Extract View State value from response. Orginial Value: 4712175963253169600:-3939968574676408262*/
	web_reg_save_param("cViewState_02",
	                   "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
	                   "RB=\" autocomplete=\"",
	                   "ORD=1",
	                   LAST);
	
	/*Extract cSelectedRow value from response. Orginial Value: 209481*/
	web_reg_save_param("cSelectedRow",
	                   "LB=checkAll_c0_dataForm:OrderListPage_entityListView:releaseDataTable\" value=\"0\" /><input type=\"hidden\" value=\"",
	                   "RB=\" id=",
	                   "ORD=1",
	                   LAST);
	
	/*Extract Dtributor ID value from response. Orginial Value: 00208674 */
	web_reg_save_param("cDistributorID",
	                   "LB=\"dataForm:OrderListPage_entityListView:releaseDataTable:0:tcId8\">",
	                   "RB=</span>",
	                   "ORD=1",
	                   LAST);
	
		/*Extract Pick List ID value from response. Original Value:0000001007*/
	web_reg_save_param("cPickListID",
	                   "LB=\"dataForm:OrderListPage_entityListView:releaseDataTable:0:pickListId\">",
	                   "RB=</span>",
	                   "ORD=1",
	                   LAST); 

	
	/* Search Sales Order */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Search_Sales_Order"));

/*	web_url("ping.jsp_6", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1479378261691", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		LAST); */

	web_custom_request("InStorePickupSolution.xhtml_2", 
		"URL={pURL}/eem/instorepickup/InStorePickupSolution.xhtml", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={cMANH_CSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Feem%252Finstorepickup%252FInStorePickupSolution.jsflps&moreActionTargetLinkconfirmPickupMessage_ActionPanel=&moreActionButtonPressedconfirmPickupMessage_ActionPanel=&dataForm%3ApufOrderId=&dataForm%3ApufOrderNbr=&dataForm%3ApufOrderType=&"
		"dataForm%3ApufDeliveryOptions=&moreActionTargetLinkpuFooter_p1=&moreActionButtonPressedpuFooter_p1=&moreActionTargetLinkpuFooter_p2=&moreActionButtonPressedpuFooter_p2=&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir=desc&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_allselectedrowskey="
		"Facility_popup_Facility_Contact_dataTable%24%3A%24{cDataTable}&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&"
		"dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey=Facility_popup_Credit_Limit_List_dataTable%24%3A%24{cDataTable}&dataForm%3ACancelReasonCode_DropDown=DM&moreActionTargetLinkRCActionPanel=&"
		"moreActionButtonPressedRCActionPanel=&dataForm%3AauditLPNIds=&moreActionTargetLinkfpp_rapanel=&moreActionButtonPressedfpp_rapanel=&moreActionTargetLinkaudit_lapanel=&moreActionButtonPressedaudit_lapanel=&dataForm%3AshipLPNIds=&moreActionTargetLinkfpp_ship_rapanel=&moreActionButtonPressedfpp_ship_rapanel=&moreActionTargetLinkship_lapanel=&moreActionButtonPressedship_lapanel=&dataForm%3Ato_StoreType=CS&dataForm%3ACreateTransferOrder_facility_InText=&dataForm%3ACreateTransferOrder_City_InText=&"
		"dataForm%3ACreateTransferOrder_ZipCode_InText=&dataForm%3ACreateTransferOrder_State_InText=&dataForm%3ApickListIdecId=&dataForm%3ApickListId=&dataForm%3AscanPickListPromptText=(scan%20pick%20list%20ID)&dataForm%3ASelectPickList_dropdown=&moreActionTargetLinkAcceptCreatePickList_btnPnl1=&moreActionButtonPressedAcceptCreatePickList_btnPnl1=&moreActionTargetLinkAcceptCreatePickList_btnPnl2=&moreActionButtonPressedAcceptCreatePickList_btnPnl2=&moreActionTargetLinkshipaddinfo_footer_panel=&"
		"moreActionButtonPressedshipaddinfo_footer_panel=&moreActionTargetLinkcolp_lapanel=&moreActionButtonPressedcolp_lapanel=&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName={cFilterName}&dataForm%3AfltrListFltrId%3Aowner={cFilterOwner}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&"
		"dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4="
		"FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-7981426&windowId=screen-7981426&defaultStoreAliasId={cStoreAliasID}&notesSaveAction=notesSaveAction&ajaxTabClicked=&inStorePickUpSolutionTabPanel_SubmitOnTabClick=true&inStorePickUpSolutionTabPanel_selectedTab=TAB_OrderList&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AradioSelect=quick&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_fltrExpColTxt=DONE&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpColState=collapsed&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_expand.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrColIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_collapse.gif&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrdropDownSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2FarrowDown.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10=Order%20nbr&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject10=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1ecId=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20=Reference%20order&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject20=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1ecId=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1={pOrderNumber}&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28=First%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject28=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29=Last%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject29=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30=Status&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject30=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30value1=%5B%5D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40=Delivery%20type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject40=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50=Order%20Type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject50=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290=Pick%20list%20ID&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject290=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290value1=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AcurrentAppliedFilterId=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonCategory=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonIndex=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_changeDefault=false&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonCategory=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonIndex=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_changeDefault=false&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AdummyToGetPrefix=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AfilterId=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Aowner=&customParams%20=windowId%3Dscreen-7981426&queryPersistParameter=%26windowId%3Dscreen-7981426&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AobjectType=INSTORE_ORDER&isJSF=true&filterScreenType=ON_SCREEN&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3Apager%3ApageInput=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerBoxValue=&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisPaginationEvent=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerAction=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_deleteHidden=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedRows=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisSortButtonClick=InStorePickupMiniDO.effectiveRankString&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AsortDir=asc&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AcolCount=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableClicked=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableResized=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3APK_0={cPK_1}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AtcDistributionOrderId={cLandingPage_DistributionID_2}&isCancel="
		"{cIsCancel_1}&isCancel={cIsCancel_2}&isCancel={cIsCancel_3}&isCancel={cIsCancel_4}&isCancel={cIsCancel_5}&isCancel={cIsCancel_6}&isCancel={cIsCancel_7}&isCancel={cIsCancel_8}&isCancel={cIsCancel_9}&isCancel={cIsCancel_10}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AorderStatusCode={cOrderStatusCode_1}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdestinationActionCode=02&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3APK_1={cPK_2}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AtcDistributionOrderId={cLandingPage_DistributionID_3}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AorderStatusCode"
		"={cOrderStatusCode_2}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3APK_2={cPK_3}&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AtcDistributionOrderId={cLandingPage_DistributionID_4}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AorderStatusCode={cOrderStatusCode_3}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AdeliveryOptionCode=01&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3APK_3={cPK_4}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AtcDistributionOrderId"
		"={cLandingPage_DistributionID_5}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AorderStatusCode={cOrderStatusCode_4}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AshipByParcel=true&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3APK_4={cPK_5}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AtcDistributionOrderId={cLandingPage_DistributionID_6}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AorderStatusCode={cOrderStatusCode_5}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AdoType=20&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3APK_5={cPK_6}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AOrderList_scorInd_11=false"
		"&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AtcDistributionOrderId={cLandingPage_DistributionID_7}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AorderStatusCode={cOrderStatusCode_6}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AdestinationActionCode=02&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3APK_6={cPK_7}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AtcDistributionOrderId={cLandingPage_DistributionID_8}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AorderStatusCode"
		"={cOrderStatusCode_7}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3APK_7={cPK_8}&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AtcDistributionOrderId={cLandingPage_DistributionID_9}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AorderStatusCode={cOrderStatusCode_8}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AdeliveryOptionCode=01&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3APK_8={cPK_9}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AtcDistributionOrderId"
		"={cLandingPage_DistributionID_10}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AorderStatusCode={cOrderStatusCode_9}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AshipByParcel=true&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3APK_9={cPK_10}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AtcDistributionOrderId={cLandingPage_DistributionID_11}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AorderStatusCode={cOrderStatusCode_10}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AdoType="
		"20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3ApnhFlag=&releaseDataTable_hdnMaxIndexHldr=9&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_pageallrowskey="
		"209355%23%3A%23209407%23%3A%23209435%23%3A%23209450%23%3A%23209483%23%3A%23209468%23%3A%23209469%23%3A%23209481%23%3A%23209482%23%3A%23209462%23%3A%23&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedIdList=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_allselectedrowskey=releaseDataTable%24%3A%24{cDataTable}&targetLink=&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonCategory=-1&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonIndex=-1&"
		"dataForm%3ATransferOrder_GrpBtnCnt_changeDefault=false&moreActionTargetLinkbuttonsInList_1=&moreActionButtonPressedbuttonsInList_1=&backingBeanName=&javax.faces.ViewState={cViewState_01}&fltrApplyFromQF=true&reRenderParent=AJAXOrderListPanel%2CAJAXOrderSummaryPanel&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_orderapply=dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_orderapply&fltrClientId="
		"dataForm%3AOrderListPage_entityListView%3Afilter_order&", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Search_Sales_Order"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_7", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1479378276946", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_8", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1479378292208", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		LAST);
	
	/*Extract View State value from response.*/
	web_reg_save_param("cViewState_03",
	                   "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
	                   "RB=\" autocomplete=\"",
	                   "ORD=1",
	                   LAST);
	
	/* Select Sales Order and Click on Confirm Pickup button */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_06_Select_Sales_Order_and_click_on_Confirm_Pickup"));

	web_custom_request("InStorePickupSolution.xhtml_3", 
		"URL={pURL}/eem/instorepickup/InStorePickupSolution.xhtml", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={cMANH_CSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Feem%252Finstorepickup%252FInStorePickupSolution.jsflps&moreActionTargetLinkconfirmPickupMessage_ActionPanel=&moreActionButtonPressedconfirmPickupMessage_ActionPanel=&dataForm%3ApufOrderId=&dataForm%3ApufOrderNbr=&dataForm%3ApufOrderType=&"
		"dataForm%3ApufDeliveryOptions=&moreActionTargetLinkpuFooter_p1=&moreActionButtonPressedpuFooter_p1=&moreActionTargetLinkpuFooter_p2=&moreActionButtonPressedpuFooter_p2=&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir=desc&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_allselectedrowskey="
		"Facility_popup_Facility_Contact_dataTable%24%3A%24{cDataTable}&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&"
		"dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey=Facility_popup_Credit_Limit_List_dataTable%24%3A%24{cDataTable}&dataForm%3ACancelReasonCode_DropDown=DM&moreActionTargetLinkRCActionPanel=&"
		"moreActionButtonPressedRCActionPanel=&dataForm%3AauditLPNIds=&moreActionTargetLinkfpp_rapanel=&moreActionButtonPressedfpp_rapanel=&moreActionTargetLinkaudit_lapanel=&moreActionButtonPressedaudit_lapanel=&dataForm%3AshipLPNIds=&moreActionTargetLinkfpp_ship_rapanel=&moreActionButtonPressedfpp_ship_rapanel=&moreActionTargetLinkship_lapanel=&moreActionButtonPressedship_lapanel=&dataForm%3Ato_StoreType=CS&dataForm%3ACreateTransferOrder_facility_InText=&dataForm%3ACreateTransferOrder_City_InText=&"
		"dataForm%3ACreateTransferOrder_ZipCode_InText=&dataForm%3ACreateTransferOrder_State_InText=&dataForm%3ApickListIdecId=&dataForm%3ApickListId=&dataForm%3AscanPickListPromptText=(scan%20pick%20list%20ID)&dataForm%3ASelectPickList_dropdown=&moreActionTargetLinkAcceptCreatePickList_btnPnl1=&moreActionButtonPressedAcceptCreatePickList_btnPnl1=&moreActionTargetLinkAcceptCreatePickList_btnPnl2=&moreActionButtonPressedAcceptCreatePickList_btnPnl2=&moreActionTargetLinkshipaddinfo_footer_panel=&"
		"moreActionButtonPressedshipaddinfo_footer_panel=&moreActionTargetLinkcolp_lapanel=&moreActionButtonPressedcolp_lapanel=&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName={cFilterName}&dataForm%3AfltrListFltrId%3Aowner={cFilterOwner}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&"
		"dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4="
		"FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-7981426&defaultStoreAliasId={cStoreAliasID}&notesSaveAction=notesSaveAction&ajaxTabClicked=&inStorePickUpSolutionTabPanel_SubmitOnTabClick=true&inStorePickUpSolutionTabPanel_selectedTab=TAB_OrderList&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AradioSelect=quick&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_fltrExpColTxt=DONE&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpColState=collapsed&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_expand.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrColIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_collapse.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrdropDownSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2FarrowDown.gif&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10=Order%20nbr&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject10=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1ecId=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20=Reference%20order&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject20=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1ecId=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1={pOrderNumber}&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28=First%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject28=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29=Last%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject29=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29value1=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30=Status&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject30=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40=Delivery%20type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject40=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50=Order%20Type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject50=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50value1=%5B%5D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290=Pick%20list%20ID&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject290=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AcurrentAppliedFilterId=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonCategory=-1"
		"&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonIndex=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_changeDefault=false&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonCategory=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonIndex=-1&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_changeDefault=false&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AdummyToGetPrefix=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AfilterId=2147483647&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Aowner=&customParams%20=%26%26%26&queryPersistParameter=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AobjectType=INSTORE_ORDER&isJSF=true&filterScreenType=ON_SCREEN&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3Apager%3ApageInput=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerBoxValue=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisPaginationEvent=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerAction=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_deleteHidden=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedRows={cSelectedRow}%23%3A%23&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedRows=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisSortButtonClick=InStorePickupMiniDO.effectiveRankString&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AsortDir=asc&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AcolCount=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableClicked=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableResized=false&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_checkAll=on&releaseDataTable_hdnMaxIndexHldr=1&checkAll_c0_dataForm%3AOrderListPage_entityListView%3AreleaseDataTable=0&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3APK_0={cPK_1}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AtcDistributionOrderId={cDistributorID}&isCancel=true&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AorderStatusCode={cOrderStatusCode_1}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AshipByParcel=true&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_pageallrowskey={cSelectedRow}%23%3A%23&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedIdList=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_allselectedrowskey=releaseDataTable%24%3A%24{cDataTable}&targetLink=&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonCategory=-1&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonIndex=-1&"
		"dataForm%3ATransferOrder_GrpBtnCnt_changeDefault=false&moreActionTargetLinkbuttonsInList_1=&moreActionButtonPressedbuttonsInList_1=&backingBeanName=&javax.faces.ViewState={cViewState_02}&dataForm%3AconfirmPickupBtn=dataForm%3AconfirmPickupBtn&fetchCustomerInfo=true&permissionsEL=CONFRMSTOREPICK&", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_06_Select_Sales_Order_and_click_on_Confirm_Pickup"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_9", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1479378307446", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t16.inf", 
		"Mode=HTML", 
		LAST);

	/* Click on Confirm button */
	
	 lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Click_On_Confirm"));

	web_custom_request("InStorePickupSolution.xhtml_4", 
		"URL={pURL}/eem/instorepickup/InStorePickupSolution.xhtml", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7981426", 
		"Snapshot=t17.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={cMANH_CSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Feem%252Finstorepickup%252FInStorePickupSolution.jsflps&dataForm%3Aispucl_SOMPaidUsing=************0026&moreActionTargetLinkconfirmPickupMessage_ActionPanel=&moreActionButtonPressedconfirmPickupMessage_ActionPanel=&dataForm%3ApufOrderId=&"
		"dataForm%3ApufOrderNbr=&dataForm%3ApufOrderType=&dataForm%3ApufDeliveryOptions=&moreActionTargetLinkpuFooter_p1=&moreActionButtonPressedpuFooter_p1=&moreActionTargetLinkpuFooter_p2=&moreActionButtonPressedpuFooter_p2=&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir=desc&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_allselectedrowskey="
		"Facility_popup_Facility_Contact_dataTable%24%3A%24{cDataTable}&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&"
		"dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey=Facility_popup_Credit_Limit_List_dataTable%24%3A%24{cDataTable}&dataForm%3ACancelReasonCode_DropDown=DM&moreActionTargetLinkRCActionPanel=&"
		"moreActionButtonPressedRCActionPanel=&dataForm%3AauditLPNIds=&moreActionTargetLinkfpp_rapanel=&moreActionButtonPressedfpp_rapanel=&moreActionTargetLinkaudit_lapanel=&moreActionButtonPressedaudit_lapanel=&dataForm%3AshipLPNIds=&moreActionTargetLinkfpp_ship_rapanel=&moreActionButtonPressedfpp_ship_rapanel=&moreActionTargetLinkship_lapanel=&moreActionButtonPressedship_lapanel=&dataForm%3Ato_StoreType=CS&dataForm%3ACreateTransferOrder_facility_InText=&dataForm%3ACreateTransferOrder_City_InText=&"
		"dataForm%3ACreateTransferOrder_ZipCode_InText=&dataForm%3ACreateTransferOrder_State_InText=&dataForm%3ApickListIdecId=&dataForm%3ApickListId=&dataForm%3AscanPickListPromptText=(scan%20pick%20list%20ID)&dataForm%3ASelectPickList_dropdown=&moreActionTargetLinkAcceptCreatePickList_btnPnl1=&moreActionButtonPressedAcceptCreatePickList_btnPnl1=&moreActionTargetLinkAcceptCreatePickList_btnPnl2=&moreActionButtonPressedAcceptCreatePickList_btnPnl2=&moreActionTargetLinkshipaddinfo_footer_panel=&"
		"moreActionButtonPressedshipaddinfo_footer_panel=&moreActionTargetLinkcolp_lapanel=&moreActionButtonPressedcolp_lapanel=&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName={cFilterName}&dataForm%3AfltrListFltrId%3Aowner={cFilterOwner}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&"
		"dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4="
		"FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-7981426&defaultStoreAliasId={cStoreAliasID}&notesSaveAction=notesSaveAction&ajaxTabClicked=&inStorePickUpSolutionTabPanel_SubmitOnTabClick=true&inStorePickUpSolutionTabPanel_selectedTab=TAB_OrderList&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AradioSelect=quick&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_fltrExpColTxt=DONE&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpColState=collapsed&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_expand.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrColIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_collapse.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrdropDownSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2FarrowDown.gif&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10=Order%20nbr&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject10=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1ecId=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20=Reference%20order&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject20=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1ecId=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1={pOrderNumber}&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28=First%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject28=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29=Last%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject29=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29value1=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30=Status&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject30=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40=Delivery%20type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject40=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50=Order%20Type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject50=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50value1=%5B%5D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290=Pick%20list%20ID&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject290=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AcurrentAppliedFilterId=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonCategory=-1"
		"&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonIndex=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_changeDefault=false&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonCategory=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonIndex=-1&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_changeDefault=false&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AdummyToGetPrefix=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AfilterId=2147483647&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Aowner=&customParams%20=%26%26%26&queryPersistParameter=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AobjectType=INSTORE_ORDER&isJSF=true&filterScreenType=ON_SCREEN&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3Apager%3ApageInput=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerBoxValue=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisPaginationEvent=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerAction=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_deleteHidden=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedRows={cSelectedRow}%23%3A%23&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedRows=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisSortButtonClick=InStorePickupMiniDO.effectiveRankString&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AsortDir=asc&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AcolCount=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableClicked=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableResized=false&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_checkAll=on&releaseDataTable_hdnMaxIndexHldr=1&checkAll_c0_dataForm%3AOrderListPage_entityListView%3AreleaseDataTable=0&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3APK_0={cSelectedRow}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AtcDistributionOrderId={cDistributorID}&isCancel=true&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AorderStatusCode=Pending%20pick%20up&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AshipByParcel=true&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_pageallrowskey={cSelectedRow}%23%3A%23&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedIdList=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_allselectedrowskey=releaseDataTable%24%3A%24{cDataTable}&targetLink=&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonCategory=-1&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonIndex=-1&"
		"dataForm%3ATransferOrder_GrpBtnCnt_changeDefault=false&moreActionTargetLinkbuttonsInList_1=&moreActionButtonPressedbuttonsInList_1=&backingBeanName=&javax.faces.ViewState={cViewState_03}&dataForm%3AcpumAccept=dataForm%3AcpumAccept&", 
		LAST);

	web_url("ping.jsp_10", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1479378322683", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t18.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Click_On_Confirm"),LR_AUTO);

	web_url("ping.jsp_11", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1479378337975", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		LAST);
		

	return 0;
}
