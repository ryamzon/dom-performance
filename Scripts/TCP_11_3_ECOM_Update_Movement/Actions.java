/*
 * Example WebSphere MQ LoadRunner script (written in Java)
 * 
 * Script Description: 
 *     This script puts a message on a queue, then gets a response message from 
 *     another queue.
 *
 * You will probably need to add the following jar files to your classpath
 *   - com.ibm.mq.jar
 *   - connector.jar
 *   - com.ibm.mq.jmqi.jar
 *   - com.ibm.mq.headers.jar
 *   - com.ibm.mq.commonservices.jar
 */
 
import lrapi.lr;
import com.ibm.mq.*;
import java.io.*;
 
public class Actions
{
    // Variables used by more than one method
    String queueMgrName = "PERF.WM.QMGR";//QA.WM.QMGR";
    String putQueueName = "WM.TO.MIF.PIX";//"TCPMQSI3.QMTCPMQSI3T.QL.INTDOM.WCS.DOM.ORD";
    String getQueueName = "WM.TO.MIF.PIX";//"TCPMQSI3.QMTCPMQSI3T.QL.INTDOM.WCS.DOM.ORD";
    
    StringBuffer idBuffer = null;
    FileWriter idFile = null;
	BufferedWriter idWriter = null;
 
    MQQueueManager queueMgr = null;
    MQQueue getQueue = null;
    MQQueue putQueue = null;
    MQPutMessageOptions pmo = new MQPutMessageOptions();
    MQGetMessageOptions gmo = new MQGetMessageOptions();
    MQMessage requestMsg = new MQMessage();
    MQMessage responseMsg = new MQMessage();
    String msgBody = null;
 
    public int init() throws Throwable {
        // Open a connection to the queue manager and the put/get queues
        try {
            // As values set in the MQEnvironment class take effect when the 
            // MQQueueManager constructor is called, you must set the values 
            // in the MQEnvironment class before you construct an MQQueueManager object.
            MQEnvironment.hostname="10.18.1.21";
            MQEnvironment.port=1416;
            MQEnvironment.channel = "SYSTEM.ADMIN.SVRCONN";
            queueMgr = new MQQueueManager(queueMgrName);
            
             idFile = new FileWriter("C:\\correction_Receipts.txt");
			idWriter = new BufferedWriter(idFile);
			idBuffer = new StringBuffer();
 
            // Access the put/get queues. Note the open options used.
            putQueue = queueMgr.accessQueue(putQueueName, MQC.MQOO_BIND_NOT_FIXED | MQC.MQOO_OUTPUT);
            getQueue= queueMgr.accessQueue(getQueueName, MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT);
            
        } catch(Exception e) {
            lr.error_message("Error connecting to queue manager or accessing queues.");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        //lr_output_message("%s",getQueue);
 
        return 0;
    }//end of init
 
    public int action() throws Throwable {
        // This is an XML message that will be put on the queue. Could do some fancy 
        // things with XML classes here if necessary.
        // The message string can contain {parameters} if lr.eval_string() is used.
       MQMessage textMessage = new MQMessage(); // creating a object textMessage
	     textMessage.format = "MQSTR";
	     MQPutMessageOptions pmo = new MQPutMessageOptions();
	     StringBuffer strBuf = new StringBuffer(); // StringBuffer is also string but varying
	     strBuf.append("<?xml version=\"1.0\" ?><tXML><Header><Source>WMIS</Source><Source_Type/><Action_Type/><Sequence_Number/><Batch_ID/><Reference_ID>"+lr.eval_string("{pTxnNumber}")+"11</Reference_ID><User_ID/><Password/><Message_Type>Update_MOVEMENT</Message_Type><Company_ID>00000001</Company_ID><Msg_Locale/></Header><Message><PIXBridge id=\"PIXBridge_"+lr.eval_string("{pDate_1}")+"\" version=\"2.2\" timestamp=\""+lr.eval_string("{pCreatedDate}")+"\"><PIX><TransactionType>606</TransactionType><TransactionCode>02</TransactionCode><TransactionNumber>"+lr.eval_string("{pTxnNumber}")+"1</TransactionNumber><SequenceNumber>1</SequenceNumber><SKUDefinition><Company>01</Company><Division>01</Division><Style>"+lr.eval_string("{pStyle}")+"</Style><StyleSuffix>"+lr.eval_string("{pSuffix}")+"</StyleSuffix></SKUDefinition><SubSKUFields><InventoryType>F</InventoryType></SubSKUFields><LocnDefinition><LocationFileID>R</LocationFileID></LocnDefinition><PIXFields><DateCreated>"+lr.eval_string("{pCreatedDate}")+"</DateCreated><CaseNumber>"+lr.eval_string("{pCaseNumber}")+"</CaseNumber><PackageBarcode>"+lr.eval_string("{pPackageBarcode}")+"</PackageBarcode><InvAdjustmentQty>"+lr.eval_string("{pInvAdjustmentQty}")+"</InvAdjustmentQty><UnitOfMeasure>1</UnitOfMeasure><InvAdjustmentType>S</InvAdjustmentType><ReceivedFrom>05</ReceivedFrom><Warehouse>03</Warehouse><ReferenceCode2>07</ReferenceCode2><ReferenceCode4>25</ReferenceCode4><Reference4>PP</Reference4><ActionCode>06</ActionCode><ProgramID>SLG1G1RP</ProgramID><JobName>QPADEV0010</JobName><JobNumber>"+lr.eval_string("{pJobNumber}")+"</JobNumber><UserID>TEMP63</UserID><As400UserID>TEMP63</As400UserID><EntryNumber>M000000852</EntryNumber></PIXFields></PIX></PIXBridge></Message></tXML>");
	          
        System.out.println(strBuf);
        requestMsg.clearMessage();
        responseMsg.clearMessage();
 
        // Create a message object and put it on the request queue
        lr.start_transaction("TCP_8_ECOM_Post_XML_Order_MQ_01_Post");
        
        try {
            pmo.options = MQC.MQPMO_NEW_MSG_ID; // The queue manager replaces the contents of the MsgId field in MQMD with a new message identifier.
            requestMsg.replyToQueueName = getQueueName; // the response should be put on this queue
            requestMsg.report=MQC.MQRO_PASS_MSG_ID; //If a report or reply is generated as a result of this message, the MsgId of this message is copied to the MsgId of the report or reply message.
            requestMsg.format = MQC.MQFMT_STRING; // Set message format. The application message data can be either an SBCS string (single-byte character set), or a DBCS string (double-byte character set). 
            requestMsg.messageType=MQC.MQMT_REQUEST; // The message is one that requires a reply.
            requestMsg.writeString(strBuf.toString()); // message payload msgBody
            putQueue.put(requestMsg, pmo);
            } 
        
        catch(Exception e) 
        {
       	lr.error_message("Error sending message.");
		lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        
        lr.end_transaction("TCP_8_ECOM_Post_XML_Order_MQ_01_Post", lr.AUTO);
 
        return 0;
    
    }//end of action
    
    public int end() throws Throwable 
    {
        // 	Close all the connections
        try 
        {
            putQueue.close();
            getQueue.close();
            queueMgr.close();
            idWriter.close();
        }
        
        catch(Exception e)
              {
            lr.error_message("Exception in closing the connections");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
              }
 
        return 0;
    }//end of end
}