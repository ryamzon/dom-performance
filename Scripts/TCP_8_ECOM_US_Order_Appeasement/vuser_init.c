/* -------------------------------------------------------------------------------
	Script Title       : 
	Script Description : 
                        
                        
	Recorder Version   : 1096
   ------------------------------------------------------------------------------- */

vuser_init()
{
	
	web_cache_cleanup();
	
	web_cleanup_cookies();
	
	web_set_max_html_param_len("9999");
	
	web_set_sockets_option("SSL_VERSION", "TLS");
	
	web_set_sockets_option("IGNORE_PREMATURE_SHUTDOWN", "1");
	
	web_set_sockets_option("CLOSE_KEEPALIVE_CONNECTIONS", "1");
	
	 
	/* Extract SAML Request value from response */
	web_reg_save_param("c_SAML_Request",
	                   "LB=name=\"SAMLRequest\" value=\"",
	                   "RB=\"/>",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	lr_save_string("TCP_08_ECOM_US_Order_Appeasement","sTestCaseName");
	
	/* Launch */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_01_Launch"));
	
	web_url("index.html", 
		"URL={pURL}/manh/index.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		LAST);

	web_set_sockets_option("SSL_VERSION", "TLS");

	web_submit_data("SSO", 
		"Action={pURL_SSO}/profile/SAML2/POST/SSO", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t2.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=SAMLRequest", "Value={c_SAML_Request}", ENDITEM, 
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_01_Launch"),LR_AUTO);
	
	lr_think_time(1);
	
	/* Login */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_02_Login"));

	web_submit_data("j_spring_security_check", 
		"Action={pURL_SSO}/j_spring_security_check", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTML", 
		"EncodeAtSign=YES", 
		ITEMDATA, 
		"Name=j_username", "Value={pUserName}", ENDITEM, 
		"Name=j_password", "Value={pPassword}", ENDITEM, 
		LAST);

	web_url("ping.jsp", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591392639", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t4.inf", 
		"Mode=HTML", 
		LAST);
	
	web_url("ping.jsp_2", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591457921", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t5.inf", 
		"Mode=HTML", 
		LAST);


	/*Possible OAUTH authorization was detected. It is recommended to correlate the authorization parameters.*/

	lr_end_transaction(lr_eval_string("{sTestCaseName}_02_Login"),LR_AUTO);

	lr_think_time(1);

	return 0;
}
