Action()
{
	/* Order Tile search */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Order_Tile_Search"));

	lr_save_string("{\"customerOrderSearchCriteria\":{\"entityType\":\"All\",\"orderNumber\":\"","p1");
	
	lr_save_string("\",\"parentOrderNumber\":\"\",\"createdFromDate\":\"\",\"createdToDate\":\"\",\"noOfRecordsPerPage\":10,\"customerInfo\":\"\",\"customerBasicInfo\":\"\",\"sortingCriterion\":{\"sortField\":\"createdDTTM\",\"sortDirection\":\"DESC\"},\"currentPageNumber\":0}}","p2");
	
	/* Extract Customer First Name value from response */
	web_reg_save_param("c_First_Name",
	                   "LB=customerFirstName\":\"",
	                   "RB=\",\"customerLastName",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	/* Extract Customer Last Name value from response */
	web_reg_save_param("c_Last_Name",
                   "LB=customerLastName\":\"",
                   "RB=\",\"customerPhone",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	/* Extract Customer Phone Number value from response */
	web_reg_save_param("c_Phone_Number",
                   "LB=\"customerPhone\":\"",
                   "RB=\",\"customerEmail",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	/* Extract Customer Emailid value from response */
	web_reg_save_param("c_Email_Id",
                   "LB=customerEmail\":\"",
                   "RB=\"},\"",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	/* Extract Customer Id value from response */
	web_reg_save_param("c_externalCustomerId",
                   "LB=customerInfo\":{\"externalCustomerId\":",
                   "RB=,\"customerFirstName",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("customerOrderAndTransactionList",
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1466591471711&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p1}{pOrder_No}{p2}", 
		LAST);

	web_url("ping.jsp_3", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591473702", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Order_Tile_Search"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591489068", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		LAST);

	/* Order Tile view order details */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_Order_Tile_View_Details"));

	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271280&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);

	web_reg_save_param("c_OrderId",
                   "LB=\"orderId\":",
                   "RB=,\"orderNumber\"",
                   "ORD=1",
                   LAST);
	
	web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591588606", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../services/olm/systemProperties/getKey?_dc=1466591588834&keyName=BING_MAP_KEY", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/customer/email.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/customer/customer_id.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/customer/phone.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/order.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/customer.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/StoreLoadMask.js?_dc=1466591589038", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/return.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/item.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/collapse/collapseArrow.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../services/olm/customerorder/customerOrderDetails?_dc=1466591589033&responseType=FullCustomerOrder&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/radio/radio_button.png", "Referer={pURL}/manh/olm/resources/css/modern.css", ENDITEM, 
		"Url=../manh/olm/resources/icons/collapse/downArrow.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/olm/resources/icons/splitter/drag_divider.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/resources/css/gray/images/grid/hd-pop.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		"Url=../manh/olm/common/utils/plugins/ToolTip.js?_dc=1466591590027", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	lr_save_string("{\"customerOrderSearchCriteria\":{\"entityType\":\"All\",\"orderNumber\":","p3");
	
	lr_save_string(",\"parentOrderNumber\":\"\",\"createdFromDate\":\"\",\"createdToDate\":\"\",\"noOfRecordsPerPage\":1000,\"customerInfo\":{\"customerFirstName\":\"","p4");
	
	lr_save_string("\",\"customField1\":\"\",\"customField2\":\"\",\""
		"customField3\":\"\",\"customField4\":\"\",\"customField5\":\"\",\"customField6\":\"\",\"customField7\":\"\",\"customField8\":\"\",\"customField9\":\"\",\"customField10\":\"\"},\"customerBasicInfo\":\"\",\"sortingCriterion\":{\"sortField\":\"createdDTTM\",\"sortDirection\":\"DESC\"},\"currentPageNumber\":0}}","p5");
		
	web_custom_request("customerOrderAndTransactionList_2", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1466591589037&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p3}{pOrder_No}{p4}{c_First_Name}\",\"customerLastName\":\"{c_Last_Name}\",\"customerFullName\":\"{c_First_Name} {c_Last_Name}\",\"customerEmail\":\"{c_Email_Id}\",\"customerPhone\":\"{c_Phone_Number}\",\"customerId\":\"\",\"externalCustomerId\":\"{c_externalCustomerId}{p5}", 
		LAST);

	lr_save_string("{\"companyParameterIdList\":{\"parameterList\":[\"CURRENCY_USED\",\"EXT_CUSTOMER_ID_GENERATION\",\"USE_DELIVERY_ZONE\",\"DEFAULT_SHIP_VIA\",\"DEFAULT_ORDER_TYPE\",\"REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION\",\"REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT\",\"CHARGE_HANDLING_STRATEGY\",\"AUTOMATIC_PROMOTION_VALUE\",\"ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS\",\"ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS\",\"DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE\",\"VIEW_STORE_LEVEL_PRICES\",\"NETWORK_LEVEL_VIEW\",\""
		"FACILITY_LEVEL_VIEW\"]}}","p6");
	
	web_custom_request("companyParameterList", 
		"URL={pURL}/services/olm/basedata/companyParameter/companyParameterList?_dc=1466591591172&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p6}", 
		LAST);

/* Added by Async CodeGen.
ID=Poll_0
ScanType = Recording

The following URLs are considered part of this conversation:
	{pURL}/sessiontracking/ping.jsp?_dc=1466591606019
	{pURL}/sessiontracking/ping.jsp?_dc=1466591621442
	{pURL}/sessiontracking/ping.jsp?_dc=1466591636799
	{pURL}/sessiontracking/ping.jsp?_dc=1466591652171

TODO - The following callbacks have been added to AsyncCallbacks.c.
Add your code to the callback implementations as necessary.
	Poll_0_RequestCB
	Poll_0_ResponseCB
 */
/*	web_reg_async_attributes("ID=Poll_0", 
		"Pattern=Poll", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591606019", 
		"PollIntervalMs=15000", 
		"RequestCB=Poll_0_RequestCB", 
		"ResponseCB=Poll_0_ResponseCB", 
		LAST); */

	web_url("ping.jsp_6", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591606019", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_Order_Tile_View_Details"),LR_AUTO);

	lr_think_time(1);

/* Removed by Async CodeGen.
ID = Poll_0
 */
	/*
 web_url("ping.jsp_7",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591621442",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t14.inf",
		"Mode=HTML",
		LAST); 
	*/

	/* Search inside order details */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Order_Search_Inside_Order_Detail"));
	
	lr_save_string("{\"customerOrderSearchCriteria\":{\"entityType\":\"All\",\"orderNumber\":\"","p7");
	
	lr_save_string("\",\"parentOrderNumber\":\"\",\"createdFromDate\":\"\",\"createdToDate\":\"\",\"noOfRecordsPerPage\":10,\"customerInfo\":\"\",\"customerBasicInfo\":\"\",\"sortingCriterion\":{\"sortField\":\"createdDTTM\",\"sortDirection\":\"DESC\"},\"currentPageNumber\":0}}","p8");

	web_custom_request("customerOrderAndTransactionList_3", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1466591629357&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p7}{pOrder_No}{p8}", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Order_Search_Inside_Order_Detail"),LR_AUTO);

	lr_think_time(1);

/* Removed by Async CodeGen.
ID = Poll_0
 */
	/*
 web_url("ping.jsp_8",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591636799",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t16.inf",
		"Mode=HTML",
		LAST); 
	*/

/* Removed by Async CodeGen.
ID = Poll_0
 */
	/*
 web_url("ping.jsp_9",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591652171",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t17.inf",
		"Mode=HTML",
		LAST); 
	*/

/* Added by Async CodeGen.
ID = Poll_0
 */
/*	web_stop_async("ID=Poll_0", 
		LAST); */
		
	/* Order view details inside Order Tile */	
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_06_Order_View_Details_Inside_Order_Tile"));

	web_custom_request("windows_2", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271280", 
		"Method=DELETE", 
		"TargetFrame=", 
		"Resource=0", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t18.inf", 
		"Mode=HTML", 
		LAST);

	web_custom_request("windows_3", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271280", 
		"Method=DELETE", 
		"TargetFrame=", 
		"Resource=0", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		LAST);

	web_custom_request("windows_4", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14272166&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t20.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);

	lr_save_string("{\"orderId\":","p9");

	lr_save_string("}","p10");
	
	web_custom_request("loadIsReturnableOrder", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1466591698512&orderId={c_OrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t21.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p9}{c_OrderId}{p10}", 
		LAST);

	web_custom_request("loadIsReturnableOrder_2", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1466591699291&orderId={c_OrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t22.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p9}{c_OrderId}{p10}", 
		LAST);

	lr_save_string("{\"itemList\":{\"itemNames\":{\"itemName\":[]}}}","p11");
	
	web_custom_request("commonDeliveryOptionsForItems", 
		"URL={pURL}/services/olm/item/commonDeliveryOptionsForItems?_dc=1466591700730&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t23.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p11}", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_06_Order_View_Details_Inside_Order_Tile"),LR_AUTO);

	lr_think_time(1);

/* Added by Async CodeGen.
ID=Poll_1
ScanType = Recording

The following URLs are considered part of this conversation:
	{pURL}/sessiontracking/ping.jsp?_dc=1466591708336
	{pURL}/sessiontracking/ping.jsp?_dc=1466591723682
	{pURL}/sessiontracking/ping.jsp?_dc=1466591739064
	{pURL}/sessiontracking/ping.jsp?_dc=1466591754472
	{pURL}/sessiontracking/ping.jsp?_dc=1466591769814
	{pURL}/sessiontracking/ping.jsp?_dc=1466591785177
	{pURL}/sessiontracking/ping.jsp?_dc=1466591800976
	{pURL}/sessiontracking/ping.jsp?_dc=1466591816331
	{pURL}/sessiontracking/ping.jsp?_dc=1466591831724
	{pURL}/sessiontracking/ping.jsp?_dc=1466591847095
	{pURL}/sessiontracking/ping.jsp?_dc=1466591862462
	{pURL}/sessiontracking/ping.jsp?_dc=1466591877830

TODO - The following callbacks have been added to AsyncCallbacks.c.
Add your code to the callback implementations as necessary.
	Poll_1_RequestCB
	Poll_1_ResponseCB
 */
	/* web_reg_async_attributes("ID=Poll_1", 
		"Pattern=Poll", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591708336", 
		"PollIntervalMs=15000", 
		"RequestCB=Poll_1_RequestCB", 
		"ResponseCB=Poll_1_ResponseCB", 
		LAST); */

	web_url("ping.jsp_10", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591708336", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t24.inf", 
		"Mode=HTML", 
		LAST);

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_11",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591723682",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t25.inf",
		"Mode=HTML",
		EXTRARES,
		"URL=../manh/olm/resources/icons/tool-sprites.gif", "Referer={pURL}/manh/olm/resources/css/modern.css", ENDITEM,
		LAST); 
	*/

	
	/* Click on Order Appeasement */
	
//	lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Order_Appeasement_Click"));

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_12",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591739064",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t26.inf",
		"Mode=HTML",
		LAST); 
	*/

/*	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Order_Appeasement_Click"),LR_AUTO);

	lr_think_time(1);

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_13",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591754472",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t27.inf",
		"Mode=HTML",
		EXTRARES,
		"URL=../services/olm/appeasements/appeasementList?_dc=1466591765623&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM,
		"URL=../services/olm/syscodes/syscodes?_dc=1466591765645&syscode=B131&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM,
		"URL=../services/olm/syscodes/syscodes?_dc=1466591765648&syscode=B131&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM,
		LAST); 
	*/

	/* Click on Add Appeasement */
	
//	lr_start_transaction(lr_eval_string("{sTestCaseName}_08_Add_Appeasement_Click"));

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_14",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591769814",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t28.inf",
		"Mode=HTML",
		LAST); 
	*/

//	lr_end_transaction(lr_eval_string("{sTestCaseName}_08_Add_Appeasement_Click"),LR_AUTO);

	lr_think_time(1);

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_15",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591785177",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t29.inf",
		"Mode=HTML",
		EXTRARES,
		"URL=../manh/olm/common/model/EventModel.js?_dc=1466591796137", "Referer={pURL}/manh/index.html", ENDITEM,
		LAST); 
	*/

	/* Click on Apply Appeasement button */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Appeasement_Apply_Click"));

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_16",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591800976",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t30.inf",
		"Mode=HTML",
		LAST); 
	*/

	lr_save_string("{\"customerOrder\":{\"orderNumber\":","p12");
	
	lr_save_string(",\"discountDetails\":{\"discountDetail\":{\"extDiscountDetailId\":\"","p13");
	
	lr_save_string("\",\"deleted\":false,\"objectReasonCode\":{\"reasonCode\":\"LO\",\"reasonCodeDescription\":\"\",\"comments\":\"\",\"isSystemDefined\":\"\"},\"parentEntityLineNumber\":\"\",\"discountValue\":\"4\",\"appliedBy\":\"\",\"appliedDate\":\"\"}},\"lastUpdatedDTTM\":\"2015-09-30 14:20:06.7\"}}","p14");
	
	web_reg_find("Text=84.45",LAST); //84.45 43.52
	
	web_custom_request("createOrUpdate", 
		"URL={pURL}/services/olm/customerorder/createOrUpdate?_dc=1466591806612&responseType=FullCustomerOrder", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t31.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p12}{pOrder_No}{p13}{pOrder_No}_1\",\"extDiscountId\":\"$Off\",\"discountType\":\"Appeasement\",\"discountAmount\":\"{pDiscountAmount}{p14}", 
		LAST);

	web_custom_request("loadIsReturnableOrder_3", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1466591808538&orderId={c_OrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t32.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p9}{c_OrderId}{p10}", 
		LAST);

	web_custom_request("loadIsReturnableOrder_4", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1466591809305&orderId={c_OrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t33.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p9}{c_OrderId}{p10}", 
		LAST);

	web_custom_request("commonDeliveryOptionsForItems_2", 
		"URL={pURL}/services/olm/item/commonDeliveryOptionsForItems?_dc=1466591810097&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t34.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p11}", 
		LAST);
	/*{pUserName}\",\"logCategory\":\"radio\",\"logLevel\":4,\"eventTimeStamp\":\"6/22/2016, 4:06:36 PM\",\"additionalInfo\":\"Selected $Off\"}]}} */

	lr_save_string("{\"events\":{\"event\":[{\"transactionId\":\"80ea6030-2296-471a-8ddd-82c33b59999d\",\"eventName\":\"radio selection\",\"screenId\":280008,\"userName\":\"","p15");
	
	lr_save_string("\",\"logCategory\":\"radio\",\"logLevel\":4,\"eventTimeStamp\":\"6/22/2016, 4:06:36 PM\",\"additionalInfo\":\"Selected $Off\"}]}}","p16");
	               
	web_custom_request("saveUserActivity", 
		"URL={pURL}/services/olm/log/saveUserActivity?_dc=1466591813655&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t35.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p15}{pUserName}{p16}", 
		LAST);

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_17",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591816331",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t36.inf",
		"Mode=HTML",
		LAST); 
	*/

	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Appeasement_Apply_Click"),LR_AUTO);

	lr_think_time(1);

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_18",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591831724",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t37.inf",
		"Mode=HTML",
		LAST); 
	*/

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_19",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591847095",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t38.inf",
		"Mode=HTML",
		LAST); 
	*/

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_20",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591862462",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t39.inf",
		"Mode=HTML",
		LAST); 
	*/

/* Removed by Async CodeGen.
ID = Poll_1
 */
	/*
 web_url("ping.jsp_21",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466591877830",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t40.inf",
		"Mode=HTML",
		LAST); 
	*/

/* Added by Async CodeGen.
ID = Poll_1
 */
/*	web_stop_async("ID=Poll_1", 
		LAST);*/


	return 0;
}
