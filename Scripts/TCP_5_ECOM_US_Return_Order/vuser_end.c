vuser_end()
{

	/* Logout */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_09_Logout"));

	web_url("user.png", 
		"URL={pURL}/manh/mps/resources/icons/64/user.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t35.inf", 
		LAST);

	web_url("default-medium-arrow.png", 
		"URL={pURL}/manh/resources/css/gray/images/button/default-medium-arrow.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", 
		"Snapshot=t36.inf", 
		LAST);

	web_url("mouse.png", 
		"URL={pURL}/manh/mps/resources/icons/mouse.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/mps-gray.css", 
		"Snapshot=t37.inf", 
		LAST);

	web_url("checkbox.png", 
		"URL={pURL}/manh/resources/css/gray/images/form/checkbox.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", 
		"Snapshot=t38.inf", 
		LAST);

	web_add_cookie("ca_JSESSIONID=y77zu0NruyTpYAJKSB6Ww4Dc; DOMAIN=njisldombat03.tcphq.tcpcorp.local.com"); 

	web_url("logout", 
		"URL={pURL}/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t39.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_09_Logout"),LR_AUTO); 
	

	return 0;
}